# dap - Data acquisistion process

a [Sails v1](https://sailsjs.com) application

### Links

- [Sails framework documentation](https://sailsjs.com/get-started)
- [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
- [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
- [Community support options](https://sailsjs.com/support)
- [Professional / enterprise options](https://sailsjs.com/enterprise)

### Version info

This app was originally generated on Thu Dec 13 2018 12:54:50 GMT+0000 (GMT) using Sails v1.1.0.

<!-- Internally, Sails used [`sails-generate@1.16.0`](https://github.com/balderdashy/sails-generate/tree/v1.16.0/lib/core-generators/new). -->

<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->

### Running the app

- Create your local mysql database with name `example`
- Open config/datastores.js, locate default.adapter.url
- Update the value to match your sql credentials 'mysql://username:password@localhost:3306/example'
- in your terminal locate the `./resource` folder, generate the rsa keys leaving password blank by executing
  `ssh-keygen -t rsa -b 4096 -f jwtRS256.key`
  `openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub`
- copy dist.env to .env
- you see both keys in the resource folder now
- Execute npm start on the terminal
- Ta da
- API doc available here https://documenter.getpostman.com/view/4672248/Rzn6w3fo
