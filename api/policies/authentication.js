module.exports = async function(req, res, next) {
  let token;

  if (req.headers && req.headers.authorization) {
    let parts = req.headers.authorization.split(' ');
    if (parts.length === 2) {
      let scheme = parts[0],
        credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      return res.json(401, { err: 'Format is Authorization: Bearer [token]' });
    }
  } else if (req.param('token')) {
    token = req.param('token');
    // We delete the token from param to not mess with blueprints
    delete req.query.token;
  } else {
    return res.json(401, {
      err: sails.config.custom.error.message.NO_AUTH_HEADER
    });
  }

  let verifiedToken = await sails.helpers
    .jwtVerify(token, res)
    .tolerate('invalidOrExpiredToken', () => {
      req.token = token; // This is the decrypted token or the payload you provided
      return false;
    });

  // if token is not valid
  if (!verifiedToken) {
    sails.log.debug(
      _.merge(
        { _message: 'token verified' },
        { _status: 'success' },
        { data: verifiedToken }
      )
    );
    return res.json(401, {
      status: 'error',
      message: sails.config.custom.error.message.INVALID_TOKEN
    });
  } else {
    req.verifiedToken = verifiedToken;
    req.me = await User.findOne({ email: verifiedToken.email });

    return next();
  }
};
