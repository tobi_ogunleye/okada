const PAGE_LIMIT = parseInt(process.env.PAGE_LIMIT); //default entity limit

module.exports = {
  friendlyName: 'Get products by search params',

  description: '',

  inputs: {
    brand: {
      type: 'ref',
      description: ' array of brands',
      required: false
    },
    limit: {
      type: 'number',
      description: 'results limits',
      defaultsTo: PAGE_LIMIT
    },
    skip: {
      type: 'number',
      description: 'results limits',
      defaultsTo: 0
    },
    category: {
      type: 'ref',
      description: ' array of category',
      required: false
    }
  },

  exits: {
    success: {
      outputFriendlyName: 'Products by search params'
    }
  },

  fn: async function(inputs) {
    // Get products by search params.
    let productsBySearchParams;
    let entities = [];
    // TODO

    //get brands by id
    let brandIds = [];
    let categoryIds = [];

    if (inputs.brand.length > 0) {
      let brandIds = await sails.helpers.getDataBrandByName.with({
        brands: inputs.brand,
        fields: ['id']
      });
      brandIds = brandIds.map(obj => {
        return obj.id;
      });
    }
    if (inputs.category.length > 0) {
      //get brands by id
      let categoryIds = await sails.helpers.getDataCategoryByName.with({
        category: inputs.category,
        fields: ['id']
      });
      categoryIds = categoryIds.map(obj => {
        return obj.id;
      });
    }

    productsBySearchParams = await Entity.find()
      .populate('brand')
      .populate('code')
      .limit(inputs.limit <= PAGE_LIMIT ? inputs.limit : PAGE_LIMIT)
      .skip(inputs.skip !== null ? inputs.skip : 0);
    // .log(sails.log.debug);

    /**
     * filter out matching attr
     */
    if (brandIds.length > 0) {
      productsBySearchParams = entities.filter(obj => {
        return _.contains(brandIds, obj.brand.id);
      });
    }
    // Send back the result through the success exit.
    return productsBySearchParams;
  }
};
