module.exports = {
  friendlyName: 'User profile',

  description: '',

  inputs: {
    userId: {
      type: 'number',
      description: 'number 1',
      required: true
    },
    res: {
      type: 'ref',
      example: 1,
      description: 'response header',
      required: true
    }
  },

  exits: {
    success: {
      description: 'All done.',
      outputFriendlyName: 'User Object ready',
      outputType: 'ref'
    },
    serverError: {
      description: 'Error creating user , Please try again'
    },
    notFound: {
      description: ''
    }
  },

  fn: async function(inputs, exits) {
    let existingUser = await User.findOne({
      id: parseInt(inputs.userId)
    });

    if (!existingUser) {
      sails.log.debug(
        _.merge(
          { _message: "Can't finding user with id " + inputs.user.id },
          { _status: 'Error' },
          { data: {} }
        )
      );
      //error response
      return inputs.res.status(500).json({
        data: {},
        status: 'error',
        message: 'Error finding user profile'
      });
    } else {
      // return user profile
      return existingUser;
    }
  }
};
