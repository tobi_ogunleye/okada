module.exports = {
  friendlyName: 'User create',

  description: '',

  inputs: {
    user: {
      type: 'ref',
      example: 'user object',
      description: 'User Object',
      required: true
    },
    res: {
      type: 'ref',
      example: 1,
      description: 'response header',
      required: true
    }
  },

  exits: {
    success: {
      description: 'All done.',
      outputFriendlyName: 'User Object ready',
      outputType: 'ref'
    },
    serverError: {
      description: 'Error creating user , Please try again'
    },
    notFound: {
      description: ''
    }
  },

  fn: async function(inputs, exits) {
    let bcrypt = require('bcryptjs');

    let userObject = {
      email: inputs.user.email || null,
      email_canonical: inputs.user.email || null,
      username_canonical: inputs.user.email || null,
      username: inputs.user.email || null,
      password: bcrypt.hashSync(
        inputs.user.password,
        sails.config.saltRounds || 10
      ),
      enabled: true
    };

    let newOrExistingUser = await User.findOrCreate(
      { email: inputs.user.email },
      userObject
    ).exec(async (err, user, wasCreated) => {
      if (err) {
        sails.log.debug(
          _.merge(
            { _message: 'Error createing user' },
            { _status: 'Error' },
            { data: err }
          )
        );
        return inputs.res.status(500).json({
          data: {
            err: err.message,
            stack: err.stack
          },
          status: 'error',
          message: "something's gone wrong when creating user"
        });
      }

      if (wasCreated) {
        user.isNewUser = true;
      } else {
        user.isNewUser = false;
      }
      //get jwt token
      let token = await sails.helpers.jwtGenerate(inputs.user, inputs.res);

      return inputs.res.ok({
        data: {
          accessToken: token
        },
        status: 'success',
        message: 'user created'
      });
    });
  }
};
