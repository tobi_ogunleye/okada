module.exports = {
  friendlyName: 'Get brand by name',

  description: '',

  inputs: {
    category: {
      type: 'ref',
      description: 'array of brands',
      required: true
    },
    fields: {
      type: 'ref',
      description: 'array of attributes to return',
      defaultsTo: null
    }
  },
  exits: {
    success: {
      outputFriendlyName: 'Brand by name'
    }
  },

  fn: async function(inputs) {
    // Get brand by name.
    let categoryByName = null;

    let categories = inputs.category.map(function(obj) {
      return obj.toLowerCase();
    });

    //get brands by name
    categoryByName =
      (await Category.find({ name: { in: categories } })) || null;

    sails.log.debug(
      _.merge(
        { _message: 'categories found:  ' + categoryByName.length },
        { _status: '' },
        { data: categoryByName }
      )
    );

    //filter out fields from data set
    if (inputs.fields !== null) {
      try {
        categoryByName = categoryByName.map(function(obj) {
          return _.pick(obj, Object.values(inputs.fields));
        });
      } catch (e) {
        sails.log.debug(
          _.merge(
            { _message: 'Error picking fields' },
            { _status: '' },
            { data: e }
          )
        );
      }
    }
    // Send back the result through the success exit.
    return categoryByName;
  }
};
