module.exports = {
  friendlyName: 'Jwt generate',

  description: '',

  inputs: {
    user: {
      type: 'ref',
      description: 'User Object',
      required: true
    },
    res: {
      type: 'ref',
      description: 'response header',
      required: true
    }
  },

  exits: {
    success: {
      description: 'All done.'
    }
  },

  fn: async function(inputs) {
    //check rsa key resource availability
    const fs = require('fs');
    let jwt = require('jsonwebtoken');
    let path = require('path');

    let privateKey;
    try {
      privateKey = fs.readFileSync(
        path.resolve(process.env.PRIVATE_KEY) || null,
        'utf8'
      );
    } catch (err) {
      sails.log.debug(
        _.merge(
          { _message: 'Error reading private key file' },
          { _status: 'Warning' },
          { data: err }
        )
      );
    }

    let i = 'M5 auth service'; // Issuer
    let s = 'iam@user.me'; // Subject
    let a = 'Client_Identity'; // Audience
    let signOptions = {
      issuer: i,
      subject: s,
      audience: a,
      expiresIn: parseInt(process.env.TOKEN_EXPIRY) || 1000, // Token Expire time
      algorithm: 'RS256' // RSASSA [ "RS256", "RS384", "RS512" ]
    };

    try {
      return jwt.sign(
        { email: inputs.user.email },
        privateKey || null, // Token Secret that we sign it with
        signOptions
      );
    } catch (error) {
      return inputs.res.status(500).json({
        data: {},
        status: 'error',
        message: 'Error generating access token, please try again later'
      });
    }
  }
};
