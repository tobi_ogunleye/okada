module.exports = {
  friendlyName: 'User update',

  description: '',

  inputs: {
    user: {
      type: 'ref',
      example: 'user object',
      description: 'User Object',
      required: true
    },
    res: {
      type: 'ref',
      example: 1,
      description: 'response header',
      required: true
    }
  },

  exits: {
    success: {
      description: 'All done.',
      outputFriendlyName: 'User Object ready',
      outputType: 'ref'
    },
    serverError: {
      description: 'Error creating user , Please try again'
    },
    notFound: {
      description: ''
    }
  },

  fn: async function(inputs, exits) {
    let bcrypt = require('bcryptjs');

    let existingUser = await User.findOne({
      id: parseInt(inputs.user.id)
    });

    if (!existingUser) {
      sails.log.debug(
        _.merge(
          { _message: 'Error finding user' },
          { _status: 'Error' },
          { data: {} }
        )
      );
      //error response
      return inputs.res.status(500).json({
        data: {},
        status: 'error',
        message: "Can't finding user with id " + inputs.user.id
      });
    } else {
      // update record and fetch
      try {
        await User.updateOne({ id: parseInt(inputs.user.id) }).set(
          _.omit(inputs.user, ['id'])
        );
      } catch (error) {
        sails.log.debug(
          _.merge(
            { _message: 'Error updating user' },
            { _status: 'Error' },
            { data: error }
          )
        );

        return inputs.res.status(500).json({
          data: {
            err: error.message,
            stack: error.stack
          },
          status: 'error',
          message: 'cant updating user with id ' + inputs.user.id
        });
      }

      return inputs.res.ok({
        status: 'success',
        message: 'user entity updated'
      });
    }
  }
};
