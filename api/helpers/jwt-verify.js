/**
 * jwToken
 *
 * @description :: JSON Webtoken Service for sails
 * @help        :: See https://github.com/auth0/node-jsonwebtoken & http://sailsjs.org/#!/documentation/concepts/Services
 */

let jwt = require('jsonwebtoken');

module.exports = {
  friendlyName: 'Verify jwt access token',

  description: '',

  inputs: {
    token: {
      type: 'string',
      example: '***********',
      description: 'access token',
      required: true
    },
    res: {
      type: 'ref'
    }
  },

  exits: {
    invalidOrExpiredToken: {
      responseType: 'expired',
      description: 'The provided token is expired, invalid, or already used up.'
    }
  },

  fn: async function(inputs, exits) {
    //check rsa key resource availability
    const fs = require('fs');
    let jwt = require('jsonwebtoken');
    let path = require('path');
    let publicKey;

    try {
      publicKey = fs.readFileSync(
        path.resolve(process.env.PUBLIC_KEY) || null,
        'utf8'
      );
    } catch (err) {
      sails.log.debug(
        _.merge(
          { _message: 'Error reading public key file' },
          { _status: 'Warning' },
          { data: err }
        )
      );
    }

    let i = 'M5 auth service'; // Issuer
    let s = 'iam@user.me'; // Subject
    let a = 'Client_Identity'; // Audience

    let signOptions = {
      issuer: i,
      subject: s,
      audience: a,
      expiresIn: parseInt(process.env.TOKEN_EXPIRY) || 1000, // Token Expire time
      algorithm: 'RS256' // RSASSA [ "RS256", "RS384", "RS512" ]
    };

    let token = jwt.verify(
      inputs.token, // The token to be verified
      publicKey, // Same token we used to sign
      signOptions,
      (err, token) => {
        if (err) {
          sails.log.debug(
            _.merge(
              { _message: 'Error verifing token' },
              { _status: 'Error' },
              { data: err }
            )
          );
          return 'invalidOrExpiredToken';
        } else {
          return token;
        }
      } //Pass errors or decoded token to callback
    );
    return token;
  }
};
