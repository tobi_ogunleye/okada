module.exports = {
  friendlyName: 'Get brand by name',

  description: '',

  inputs: {
    brands: {
      type: 'ref',
      description: 'array of brands',
      required: true
    },
    fields: {
      type: 'ref',
      description: 'array of attributes to return',
      defaultsTo: null
    }
  },
  exits: {
    success: {
      outputFriendlyName: 'Brand by name'
    }
  },

  fn: async function(inputs) {
    // Get brand by name.
    let brandByName = null;

    let brands = inputs.brands.map(function(obj) {
      return obj.toLowerCase();
    });

    //get brands by name
    brandByName = (await Brand.find({ name: { in: brands } })) || null;

    sails.log.debug(
      _.merge(
        { _message: 'brands found:  ' + brandByName.length },
        { _status: '' },
        { data: brandByName }
      )
    );

    //filter out fields from data set
    if (inputs.fields !== null) {
      try {
        brandByName = brandByName.map(function(obj) {
          return _.pick(obj, Object.values(inputs.fields));
        });
      } catch (e) {
        sails.log.debug(
          _.merge(
            { _message: 'Error picking fields' },
            { _status: '' },
            { data: e }
          )
        );
      }
    }
    // Send back the result through the success exit.
    return brandByName;
  }
};
