/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const bcrypt = require('bcryptjs');
const moment = require('moment');
const uuid = require('uuid/v4');

module.exports = {
  //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
  //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
  //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

  attributes: {
    id: {
      type: 'number',
      autoIncrement: true
    },
    username: {
      type: 'string',
      required: false
    },
    username_canonical: {
      type: 'string',
      required: false
    },
    email: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 200,
      example: 'a@b.com'
    },
    email_canonical: {
      type: 'string',
      required: true
    },
    enabled: {
      type: 'boolean',
      required: false
    },
    // salt: {
    //     type: 'string',
    //     default: 'NULL::character varying',
    //     required: true
    // },
    password: {
      type: 'string',
      required: true
    },
    // last_login: {
    //   type: 'string',
    //   columnType: 'datetime'
    // },

    confirmation_token: {
      type: 'string'
    },
    // password_requested_at: {
    //   type: 'string',
    //   columnType: 'datetime',
    //   required: false
    // },
    roles: {
      type: 'string',
      defaultsTo: 'a:0:{}'
    },
    firstname: {
      type: 'string',
      allowNull: false
    },
    lastname: {
      type: 'string',
      allowNull: false
    },
    // biography: {
    //     type: 'string',
    //     required: true
    // },
    // gender: {
    //     type: 'string',
    //     required: true
    // },
    locale: {
      type: 'string',
      required: false
    },
    phone: {
      type: 'string',
      required: false,
      allowNull: true
    },
    // facebook_uid: {
    //     type: 'string',
    //     default: 'NULL::character varying',
    //     required: true
    // },
    // facebook_name: {
    //     type: 'string',
    //     default: 'NULL::character varying',
    //     required: true
    // },
    // facebook_data: {
    //     type: 'string',
    //     required: true
    // }
    // twitter_uid: {
    //     type: 'string',
    //     default: 'NULL::character varying',
    //     required: true
    // },
    // twitter_name: {
    //     type: 'string',
    //     default: 'NULL::character varying',
    //     required: true
    // },
    // twitter_data: {
    //     type: 'string',
    //     required: true
    // },
    // gplus_uid: {
    //     type: 'string',
    //     default: 'NULL::character varying',
    //     required: true
    // },
    // gplus_name: {
    //     type: 'string',
    //     default: 'NULL::character varying',
    //     required: true
    // },
    // gplus_data: {
    //     type: 'string',
    //     required: true
    // },
    two_step_code: {
      type: 'string',
      allowNull: false
    },
    uuid: {
      type: 'string',
      allowNull: false
    },
    createdAt: {
      type: 'string',
      columnType: 'datetime',
      allowNull: true,
      defaultsTo: moment().format('YYYY-MM-DD HH:mm:ss')
    },
    updatedAt: {
      type: 'string',
      columnType: 'datetime',
      allowNull: true,
      defaultsTo: moment().format('YYYY-MM-DD HH:mm:ss')
    }
  },
  tableName: 'user',

  customToJSON: function() {
    return _.omit(this, ['password']);
  },
  beforeCreate: async function(user, cb) {
    user.uuid = uuid();
    user.roles = 'a:0:{}';
    user.locale = process.env.DEFAULT_LOCALE;
    cb(null);
  },
  beforeUpdate: async function(user, cb) {
    user.updatedAt = moment().format('YYYY-MM-DD HH:mm:ss');
    cb(null);
  }
};
