/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  addUser: async function(req, res, next) {
    let errorMessage = null;
    let user = {};

    req.validate(
      [
        { email: 'email' }, // (required)
        { 'phone?': 'numeric' }, // (optional)
        { password: 'string' } // (required) TODO: validation
        // { firstname: ['string', 'toLowercase'] }, // (required) 'firstname' as STRING type and convert to LOWERCASE
        // { lastname: ['string', 'toLowercase'] } // (required) 'firstname' as STRING type and convert to LOWERCASE
      ],
      false,
      function(err, params) {
        if (err) {
          errorMessage = {
            data: err,
            status: 'error',
            message: err.message
          };
        } else {
          user = params; // {id: '1234', firstname: "John", lastname: "doe"}
        }
      }
    );

    if (errorMessage !== null) {
      res.badRequest(errorMessage);
    } else {
      await sails.helpers.userCreate(user, res);
    }
  },
  updateUser: async function(req, res, next) {
    let errorMessage = null;
    let user = {};

    req.validate(
      [
        { id: 'numeric' }, // (required)
        { email: 'email' }, // (required)
        { 'phone?': 'numeric' }, // (optional)
        { password: 'string' } // (required) TODO: validation
        // { firstname: ['string', 'toLowercase'] }, // (required) 'firstname' as STRING type and convert to LOWERCASE
        // { lastname: ['string', 'toLowercase'] } // (required) 'firstname' as STRING type and convert to LOWERCASE
      ],
      false,
      function(err, params) {
        if (err) {
          errorMessage = {
            data: err,
            status: 'error',
            message: err.message
          };
        } else {
          user = params; // {id: '1234', firstname: "John", lastname: "doe"}
        }
      }
    );

    if (errorMessage !== null) {
      res.badRequest(errorMessage);
    } else {
      await sails.helpers.userUpdate(user, res);
    }
  },
  getUserProfile: async function(req, res, next) {
    let errorMessage = null;
    let user = {};

    req.validate(
      [
        { id: 'numeric' }, // (required)
        { email: 'email' }, // (required)
        { 'phone?': 'numeric' }, // (optional)
        { password: 'string' } // (required) TODO: validation
        // { firstname: ['string', 'toLowercase'] }, // (required) 'firstname' as STRING type and convert to LOWERCASE
        // { lastname: ['string', 'toLowercase'] } // (required) 'firstname' as STRING type and convert to LOWERCASE
      ],
      false,
      function(err, params) {
        if (err) {
          errorMessage = {
            data: err,
            status: 'error',
            message: err.message
          };
        } else {
          user = params; // {id: '1234', firstname: "John", lastname: "doe"}
        }
      }
    );

    if (errorMessage !== null) {
      res.badRequest(errorMessage);
    } else {
      await sails.helpers.userUpdate(user, res);
    }
  }
};
