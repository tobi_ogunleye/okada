/**
 * SearchController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  getResult: async function(req, res, next) {
    let searchParams = {};
    let errorMessage = null;
    req.validate(
      [
        // { filter: ['?array', 'toLowercase'] }, // (required)
        // { text: ['?string', 'toLowercase'] }, // (required)
        { 'brand?': ['array'] }, // (required) TODO: validation
        { 'category?': ['array'] }, // (optional)
        { 'limit?': ['numeric'] }, // (optional)
        { 'offset?': ['numeric'] } // (optional)
        // { brand: ['string', 'toLowercase'] }, // (required) TODO: validation
        // { color: ['string', 'toLowercase'] }
        // { firstname: ['string', 'toLowercase'] }, // (required) 'firstname' as STRING type and convert to LOWERCASE
        // { lastname: ['string', 'toLowercase'] } // (required) 'firstname' as STRING type and convert to LOWERCASE
      ],
      false,
      function(err, params) {
        if (err) {
          errorMessage = {
            data: err,
            status: 'error',
            message: err.message
          };
        } else {
          searchParams = params; // {id: '1234', firstname: "John", lastname: "doe"}
        }
      }
    );
    if (errorMessage !== null) {
      res.badRequest(errorMessage);
    } else if (req.params === {}) {
      res.badRequest('No search params present');
    } else {
      console.log(searchParams);
      let result = null;
      result = await sails.helpers.getProductsBySearchParams.with({
        brand: searchParams.brand || [],
        limit: searchParams.limit || 0,
        skip: searchParams.offset || 0,
        category: searchParams.category || []
      });
      res.ok({ data: result, status: 'error', message: '' });
    }
  }
};
