/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {
  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)

  let bcrypt = require('bcryptjs');
  let faker = require('faker');

  if ((await User.count()) > 0) {
    return;
  }

  // sample user
  for (let index = 0; index < 5; index++) {
    let email = faker.internet.email();
    await User.create({
      email: email || null,
      email_canonical: email || null,
      username_canonical: email || null,
      username: email || null,
      password: bcrypt.hashSync(email, sails.config.saltRounds || 10),
      enabled: true
    });
  }

  //brand

  let brand = await Brand.create({
    name: 'okada' || null,
    header: faker.lorem.sentence() || null,
    desc: faker.lorem.sentences() || null
  }).fetch();

  // add category and category attribute
  for (let index = 0; index < 5; index++) {
    let category = await Category.create({
      name: faker.commerce.department()
    }).fetch();

    await CategoryAttribute.create({
      title: faker.commerce.department(),
      categoryId: category.id
    });

    for (let index = 0; index < 5; index++) {
      let code = await Code.create({
        code: faker.finance.account(),
        barCode: faker.finance.bitcoinAddress()
      }).fetch();

      let entity = await Entity.create({
        name: faker.commerce.productName() || null,
        description: faker.hacker.phrase() || null,
        extraInfo: faker.lorem.sentence() || null,
        code: code.id,
        brand: brand.id
      }).fetch();

      await EntityAttribute.create({
        name: faker.commerce.productName() || null,
        value: faker.hacker.phrase() || null,
        entityId: entity.id || null
      });

      await Entity.addToCollection(entity.id, 'categories', category.id);
    }
  }
  // ```
};
